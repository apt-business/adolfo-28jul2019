# Adolfo - 28 Jul 2019 9:00 AM CEST Time

## Installation

```
# In one terminal start the backend
cd packages/au-api
yarn build && yarn start

# In a second terminal start the app
cd ../au-app
yarn start
```

## Testing

```
yarn test
```

## Components preview

```
yarn storybook
```

## Security

BAsed on the requirements of this application we can divide the security factors in three areas:

- **General application security risks (a.k.a GASR, see [here(OWASP)](https://www.owasp.org/index.php/Top_10-2017_Top_10))**
- **Frontend libraries vulnerabilities (in our case derived from React usage, a.k.a RV)**
- **Especific vulnerabilities associated with "Unrestricted File Upload" (a.k.a UFU see [here(OWASP)](https://www.owasp.org/index.php/Unrestricted_File_Upload))**.

  > There are really two classes of problems here. The first is with the file metadata, like the path and file name. These are generally provided by the transport, such as HTTP multi-part encoding. This data may trick the application into overwriting a critical file or storing the file in a bad location. You must validate the metadata extremely carefully before using it.

  > The other class of problem is with the file size or content. The range of problems here depends entirely on what the file is used for.

#### Security concerns have been addressed

- [RV]: Injecting child nodes: If for some reason you parsed some user input into JSON data, and injected in the react children, you could add `dangerouslySetInnerHTML` and then in HTML write malicious code in a script tag [fixed here](https://github.com/facebook/react/pull/4832?source=post_page---------------------------).
- [RV]: Dinamycally create element. We do not do this.
- [RV]: Injecting props. Mostly the same as the first one in this list, but in this case for props. `{"dangerouslySetInnerHTML" : { "__html": "<img src=x/ onerror=’alert (localStorage.access_token)’>"}}`. We do not do this.
- [RV]: Eval based injection. WE do not call eval at all.
- [RV]: Potential risks derived from SSR (like no escaping things. [See here](https://redux.js.org/recipes/server-rendering#inject-initial-component-html-and-state)). We do not do SSR.
- [RV]: Typical XSS - Dinamycally generate HREF or FORMACTION attributes or again `dangerouslySetInnerHTML` content, allowing the user to write: `javascript:...`. We do not do this.

- [UFU]: Metadata -> overwritting and risky location. We store files in DB where it creates hex names on local folder in file system, and the DB should not have execution rights, just in case. We have introduced checks in both frontend and backed for assuring
- [UFU]: File size -> We have introduced validation inside the application (see UploadButton JEST test) not allowing huge files and therefore not compromising the backend. Also multer has enabled this security, just in case someone attacks our server from other place than our app.
- [UFU]: File content -> We store the file's bytes in a DB, so no execution problems here.

- [GASR]: Injection: Not tested, do not think it applies because we are not interpreting anything at backend
- [GASR]: Btoken Authentication: Not implemented because, it is an open service
- [GASR]: Sensitivie data exposure. The usage of the service is public so, it does not apply.
- [GASR]: XML External Entities. We do not process XML.
- [GASR]: Broken Access Control. It is public, do it does not apply.
- [GASR]: Cross Site scripting (studied cases based on react perspective)
- [GASR]: Using components with known vulnerabilities (studied as well and we are not using them).

- [EXTRA]: DDOS and brute force attacks. Mitigated with express-rate-limit library

#### Security concerns have not been addressed

- [GASR]: Insecure Desirialization. Not tested deeply if bytes when they are stored to db can trigger something. I need to research (out of scope).
- [GASR]: Security Misconfiguration. Not implemented HTTPS for example.
- [GASR]: Insufficient Logging & Monitoring (I have not implemented logging at all).

## Improvements

- Improve UX and UI of the search results
- Improve UI about how the search input is cleared
- Create DOM test for assuring given a mocked list of files to render correctly the UI
- Create DOM test for assuring deletion of files work as expected
- Create TESt for checking the search component
- Include a puppeter test for seeing the use cases in action
- Create a module based on the picture entity for having the backend more modular
- Take a look into HTTP code returned from endpoints
- Allow upload multiple files
- Work on showing progress bar when uploading
- Load config with endpoints based on different profiles (test, dev, production), can be easily done doing something like node-config does.
- Implement a proper Error Page in `ErrorBoundary` component and react-router for redirecting to main view creating a new tree
- Give a spin into the Access-Control-Allow-Credentials
- ~~Give a spin into imageFiler backend function to make it more robust. Maybe analying internals?~~
- Investigate how to send specific cookies for allowing to delete files only to the person who has uploaded them

## Libraries

- **Material UI:**
  This library brings you some very powerful utilities for developing faster UIs. All they components are well tested, including image snapshots. Also it provides very powerful features like Box model, automatic media queries, CSS-IN-JS solution for interating easily with theme.

  Its concept of theme is very powerful allowing separate semantic UI design approach leaving your components clean way, allowing designers to interact and modify things in an easier way. No need to go deep in code, just update the theme! Long life to the theme and semantic UI developing approach!

- **Jest:**
  I selected this because it provides out of the box solutions for all kind of tests you want to do (integration, unit), and also it provides a good integration for testing react applications, with its own assertion library version. It has a good integration with Puppeteer as well.
  Jest also offers a super efficient mock options, making easier to test if functions are called, or override some implementations for testing features.
  I suggest to take a look for example in: [UploadButton/index.dom.spec.tsx](./packages/au-app/src/components/forms/UploadButton/index.dom.spec.tsx#lines-24)

- **Lerna:**
  I am using this tool combined with yarn workspaces feature for having a clear separation of layers in this project. The advantages of using monorepos are well known but I am thinking a step further, believing this is the right approach to move into a microservices architecture (allowing the backend to scale easily if needed)

- **Storybook:**
  I am using storybook for developing components because it is super fast re-rendering changes. Also it has a great integration with potential image checker libraries like chromatic, doing the connection by taking advantage of JEST snapshots.

- **express-rate-limit**
  Library for reducing the brut force DDos attacks.

- **cors**
  Library for reducing potentital vector attacks reducing origins which can use the app

- **multer**
  Most famous middleware library for handling multipart/form-data

- **LokiJs**
  Fast in-memory document datastore for node.js

## API

The api is very simple, it has been built based on the 'picture' term because it represents the domain model of the stored information, and also the 'find' term, due the API extension functionlaity implemented.

```
### POST /picture
It uploads a single file, it does not accept specific parameters and returns the File metada of the stored file.

It an exception is raised it returns status 400.

### GET /pictures
It returns the metada data information of the stored files. It does not accept specific parameters.

It an exception is raised it returns status 400.


### GET /find/:term
This endpoint looks if the stored files in the DB matches with the specified term. It returns 202 or 404 based if there is a match or not.

It an exception is raised it returns status 400.


### DELETE /picture/:id
This endpoint removes from DB a specific file identified by the :id parameter. It returns 204 when finished correctly the operation.

It an exception is raised it returns status 400.

```

---

## Other notes

I think is worth to mention latest React features introduced in this project

- React Optimizations: We have used React.memo and the useCallback hook for memoizing functions and run shallow comparision in some components. Check memo [here](./packages/au-app/src/routes/files/components/FileList/File.tsx#lines-22), and also check useCallback [here](./packages/au-app/src/components/forms/SearchInput.tsx#lines-38)

- React Hooks: The application has been created using hooks, you can navigate throught the components and you will see the most common ones: `useEffect` `useState` `useCallback`. There is an exception in the [ErrorBoundary](./packages/au-app/src/components/errors/ErrorBoundary.tsx#lines-15) component, which at this point the componentDidCatch lifecyle method has no [its hook version](https://reactjs.org/docs/hooks-faq.html#do-hooks-cover-all-use-cases-for-classes)

- For checking CSS (I followed the [flexbox](https://css-tricks.com/snippets/css/a-guide-to-flexbox/) model, you can check the CSS-IN-JS solution [I have applied](./packages/au-app/src/components/layout/PageLayout.tsx#lines-9) or the "more" [react variant](./packages/au-app/src/routes/files/components/FileList/File.tsx#lines-13))

- For increasing productivity I have added auto linting and auto testing in `pre-push` hook using [husky](https://github.com/typicode/husky).

- TypeScript included!!

```

```
