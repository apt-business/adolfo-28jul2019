export function randomString(size: number): string {
  const alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  return Array.from({ length: size })
    .map(() => alphabet[Math.floor(Math.random() * alphabet.length)])
    .join('');
}
