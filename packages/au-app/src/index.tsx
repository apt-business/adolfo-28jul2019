import React from 'react';
import ReactDOM from 'react-dom';
import { globalStyles } from './theme/globalStyles';
import FilesView from './routes/files';
import AwesomeUploaderThemeProvider from './theme/AuThemeProvider';
import ErrorBoundary from './components/errors/ErrorBoundary';
import { PicturesProvider } from './components/context/PicturesProvider';

const App = (): JSX.Element => (
  <ErrorBoundary>
    <AwesomeUploaderThemeProvider injectStyles={globalStyles}>
      <PicturesProvider>
        <FilesView />
      </PicturesProvider>
    </AwesomeUploaderThemeProvider>
  </ErrorBoundary>
);

ReactDOM.render(<App />, document.getElementById('root'));
