import * as React from 'react';
import { storiesOf } from '@storybook/react';
import FilesView from './index';
import { ThemeDecorator } from '../../utils/storybook';

storiesOf('Awesome Upload App|Views', module)
  .addDecorator(ThemeDecorator)
  .add('Files', () => <FilesView />);
