import React from 'react';
import { Box } from '@material-ui/core';
import PageLayout from '../../components/layout/PageLayout';
import FileActions from './components/FileActions';
import FileList from './components/FileList';
import FileInfo from './components/FileInfo';
import { PicturesContext, Picture } from '../../components/context/PicturesProvider';
import { REMOVE_PICTURE_ENDPOINT } from '../../utils/endpoints';

const FilesView: React.FC = (): JSX.Element => {
  const picturesContext = React.useContext(PicturesContext);
  const { pictures } = picturesContext;
  const number = pictures.length;
  const totalSize = pictures.reduce((total: number, pic: Picture): number => total + pic.size, 0);

  function onDelete(id: string): () => void {
    return () => {
      fetch(`${REMOVE_PICTURE_ENDPOINT}${id}`, {
        method: 'DELETE',
      })
        .catch(error => console.error('Error:', error)) // eslint-disable-line
        .then(() => {
          picturesContext.update();
        });
    };
  }

  function onUpload(): void {
    picturesContext.update();
  }

  return (
    <PageLayout>
      <FileActions onUpload={onUpload} />
      <Box marginBottom={4} />
      <FileInfo numberPictures={number} totalSize={totalSize} />
      <Box marginBottom={4} />
      <FileList onDelete={onDelete} pictures={pictures} />
    </PageLayout>
  );
};

export default FilesView;
