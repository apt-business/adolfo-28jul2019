import * as React from 'react';
import { Grid, Typography } from '@material-ui/core';
import SearchInput from '../../../components/forms/SearchInput';
import UploadImgButton from '../../../components/forms/UploadButton';
import { UPLOAD_URL, FIND_PICTURE_ENDPOINT } from '../../../utils/endpoints';

interface Props {
  readonly onUpload: () => void;
}

const FileActions = ({ onUpload }: Props): JSX.Element => {
  const [result, setResult] = React.useState('');

  function onSearch(term: string): void {
    fetch(`${FIND_PICTURE_ENDPOINT}${term}`)
      .then(response => {
        if (response.status === 202) {
          setResult(`File ${term} has been found :) `);
          return;
        }

        setResult(`File ${term} has not been found :) `);
      })
      .catch(() => {
        setResult(`File ${term} has not been found :( )`);
      });
  }

  return (
    <Grid container alignItems="center">
      <Grid item xs={12} sm={8}>
        <SearchInput delay={500} onSearch={onSearch} />
        <Typography>{result}</Typography>
      </Grid>
      <Grid item xs={12} sm={4}>
        <UploadImgButton endpoint={UPLOAD_URL} onUpload={onUpload} />
      </Grid>
    </Grid>
  );
};

export default FileActions;
