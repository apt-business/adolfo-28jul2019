import * as React from 'react';
import { Grid, makeStyles, Theme } from '@material-ui/core';
import File from './File';
import { Picture } from '../../../../components/context/PicturesProvider';

interface Props {
  readonly onDelete: (name: string) => () => void;
  readonly pictures: ReadonlyArray<Picture>;
}

const useStyles = makeStyles((theme: Theme) => ({
  file: {
    paddingBottom: `${theme.spacing(6)}px`,
    '&:nth-child(3n - 1)': {
      [theme.breakpoints.up('sm')]: {
        paddingLeft: `${theme.spacing(3)}px`,
        paddingRight: `${theme.spacing(3)}px`,
      },
    },
  },
}));

const Files = ({ onDelete, pictures }: Props): JSX.Element => {
  const classes = useStyles();

  return (
    <Grid container>
      {pictures.map((pic: Picture, index: number) => (
        <Grid key={pic.id} item xs={12} sm={4} className={classes.file}>
          <File name={pic.name} weight={pic.size / 1024} onDelete={onDelete(pic.id)} />
        </Grid>
      ))}
    </Grid>
  );
};

export default Files;
