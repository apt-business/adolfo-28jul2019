import * as React from 'react';
import { Button, Box, Typography } from '@material-ui/core';

interface FileProps {
  readonly name: string;
  readonly weight: number;
  readonly onDelete: () => void;
}

const File = ({ name, weight, onDelete }: FileProps): JSX.Element => (
  <Box padding={1.5} border={1} borderColor="primary.main" display="flex" flexDirection="column">
    <Typography variant="h6" noWrap>{`File name: ${name}`}</Typography>
    <Box paddingTop={2} display="flex" alignItems="center" justifyContent="space-between">
      <Typography noWrap>{`${weight} kb`}</Typography>
      <Box paddingRight={1} />
      <Button size="small" color="primary" variant="contained" onClick={onDelete}>
        Delete
      </Button>
    </Box>
  </Box>
);

export default React.memo(File);
