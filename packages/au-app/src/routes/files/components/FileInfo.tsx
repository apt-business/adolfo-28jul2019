import * as React from 'react';
import { Grid } from '@material-ui/core';

interface Props {
  readonly numberPictures: number;
  readonly totalSize: number;
}

const FileInfo = ({ numberPictures, totalSize }: Props): JSX.Element => (
  <Grid container>
    <Grid item xs={12} sm={8}>
      {`${numberPictures} Documents`}
    </Grid>
    <Grid item xs={12} sm={4}>
      {`Total size: ${totalSize / 1024} Kb`}
    </Grid>
  </Grid>
);

export default FileInfo;
