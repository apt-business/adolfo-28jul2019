export const UPLOAD_URL = 'http://localhost:3002/picture';
export const GET_PICTURES_ENDPOINT = 'http://localhost:3002/pictures';
export const REMOVE_PICTURE_ENDPOINT = 'http://localhost:3002/picture/';
export const FIND_PICTURE_ENDPOINT = 'http://localhost:3002/find/';
