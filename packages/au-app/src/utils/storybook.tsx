import * as React from 'react';
import AwesomeUploaderThemeProvider from '../theme/AuThemeProvider';
import { globalStyles } from '../theme/globalStyles';
import ErrorBoundary from '../components/errors/ErrorBoundary';
import { PicturesProvider } from '../components/context/PicturesProvider';

export const ThemeDecorator = (storyFn: any): JSX.Element => (
  <ErrorBoundary>
    <AwesomeUploaderThemeProvider injectStyles={globalStyles}>
      <PicturesProvider>{storyFn()}</PicturesProvider>
    </AwesomeUploaderThemeProvider>
  </ErrorBoundary>
);
