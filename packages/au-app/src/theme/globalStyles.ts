import { makeStyles, Theme } from '@material-ui/core';

export const globalStyles = makeStyles((theme: Theme) => ({
  '@global': {
    '*': {
      boxSizing: 'inherit',
      WebkitFontSmoothing: 'antialiased', // Antialiasing.
      MozOsxFontSmoothing: 'grayscale', // Antialiasing.
    },
    '*::before, *::after': {
      boxSizing: 'inherit',
    },
    body: {
      margin: '0',
      padding: '0',
      fontFamily: 'sans-serif',
      boxSizing: 'border-box',
      backgroundColor: theme.palette.background.default,
    },
    '#root': {
      position: 'absolute',
      top: '0',
      bottom: '0',
      right: '0',
      left: '0',
    },
  },
}));
