import * as React from 'react';
import { TextField, makeStyles } from '@material-ui/core';

interface SearchInputProps {
  readonly onSearch: (term: string) => void;
  readonly delay: number;
}

const useStyles = makeStyles({
  textField: {
    width: '400px',
  },
});
const EMPTY_SEARCH_VALUE = '';

function SearchInput({ onSearch, delay }: SearchInputProps): JSX.Element {
  const classes = useStyles();
  const [value, setValue] = React.useState('');
  const [debouncedValue, setDebouncedValue] = React.useState(EMPTY_SEARCH_VALUE);

  React.useEffect(() => {
    const timeout = setTimeout(() => {
      setDebouncedValue(value);
    }, delay);

    return () => {
      clearTimeout(timeout);
    };
  }, [delay, value]);

  React.useEffect(() => {
    if (debouncedValue === EMPTY_SEARCH_VALUE) {
      return;
    }
    onSearch(debouncedValue);
  }, [debouncedValue, onSearch]);

  const onChange = React.useCallback((event: React.ChangeEvent<HTMLInputElement>): void => {
    setValue(event.target.value);
  }, []);

  return (
    <TextField
      id="files-search"
      label="Debounced files search input"
      value={value}
      onChange={onChange}
      className={classes.textField}
      margin="normal"
    />
  );
}

export default SearchInput;
