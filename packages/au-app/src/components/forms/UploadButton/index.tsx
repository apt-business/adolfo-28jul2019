import * as React from 'react';
import { makeStyles, Button } from '@material-ui/core';
import { uploadFile, hasImageType, hasCorrectSize } from './uploader';

interface Props {
  readonly endpoint: string; //http://localhost:3002/file/upload
  readonly onUpload: () => void;
}

const useStyles = makeStyles({
  input: {
    display: 'none',
  },
});

function UploadImgButton({ endpoint, onUpload }: Props): JSX.Element {
  const classes = useStyles();

  const onFileUploaded = React.useCallback(
    (event: React.ChangeEvent<HTMLInputElement>): void => {
      const { files } = event.target;
      if (!files) return;

      const file = files[0];
      if (!hasCorrectSize(file)) {
        return;
      }
      if (!hasImageType(file)) {
        return;
      }

      const formData = new FormData();
      formData.append('file', files[0]);

      uploadFile(endpoint, formData, onUpload);
    },
    [endpoint, onUpload],
  );

  return (
    <Button component="label">
      Upload File
      <input type="file" accept="image/jpg, image/png" className={classes.input} onChange={onFileUploaded} />
    </Button>
  );
}

export default UploadImgButton;
