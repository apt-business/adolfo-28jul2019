// import axios from 'axios';

export const hasImageType = (file: File): boolean => {
  return /image\/(jpe?g|png)$/i.test(file.type);
};

export const hasCorrectSize = (file: File): boolean => {
  const tenMbInBytes = 10 * 1024 * 1024;

  return file.size < tenMbInBytes;
};

export const uploadFile = async (
  endpoint: string,
  formData: FormData,
  callback: () => void,
): Promise<void> => {
  fetch(endpoint, {
    method: 'POST',
    body: formData,
  })
    .then(response => response.json())
    .catch(error => console.error('Error:', error)) // eslint-disable-line
    .then(response => {
      // eslint-disable-next-line
      console.log('Success:', JSON.stringify(response));
      callback();
    });
};
