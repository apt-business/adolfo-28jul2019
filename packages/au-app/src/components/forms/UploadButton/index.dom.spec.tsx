import React from 'react';
import ReactDOM from 'react-dom';
import UploadButton from './index';
import { act, Simulate } from 'react-dom/test-utils';
import * as uploader from './uploader';
import { randomString } from '../../../test/fileGenerator';
import { UPLOAD_URL } from '../../../utils/endpoints';

describe('Forms :: Upload File Input', () => {
  describe('with file type restriction', () => {
    let div: HTMLDivElement;
    beforeEach(() => {
      div = document.createElement('div');
      act(() => {
        ReactDOM.render(<UploadButton endpoint={UPLOAD_URL} onUpload={() => {}} />, div);
      });
    });

    afterEach(() => {
      ReactDOM.unmountComponentAtNode(div);
    });

    it('stores pictures', async () => {
      // GIVEN - File
      const uploadFileSpy = jest.spyOn(uploader, 'uploadFile');

      const input = div.querySelector('input');
      if (!input) throw new Error();

      const file = new File(['(⌐□_□)'], 'chucknorris.png', {
        type: 'image/png',
      });

      //WHEN - Simulate file has been uploaded
      act(() => {
        Simulate.change(input, { target: { files: [file] } } as any);
      });

      // THEN
      expect(uploadFileSpy).toHaveBeenCalled();
      expect(uploadFileSpy).not.toThrowError();
      uploadFileSpy.mockClear();
    });

    it('do not send other file types', async () => {
      // GIVEN - File
      const imgTypeValidatorSpy = jest.spyOn(uploader, 'hasImageType');
      const uploadFileSpy = jest.spyOn(uploader, 'uploadFile');

      const input = div.querySelector('input');
      if (!input) throw new Error();

      // create a text file
      const file = new File(['(⌐□_□)'], 'chucknorris.pdf', {
        type: 'application/pdf',
      });

      //WHEN - Simulate file has been uploaded
      act(() => {
        Simulate.change(input, { target: { files: [file] } } as any);
      });

      // THEN
      expect(uploadFileSpy).not.toHaveBeenCalled();
      expect(imgTypeValidatorSpy).toHaveBeenCalledWith(file);
      expect(imgTypeValidatorSpy).toHaveReturnedWith(false);
      uploadFileSpy.mockClear();
      imgTypeValidatorSpy.mockClear();
    });

    it('do not send large files', async () => {
      // GIVEN - File
      const MOCKED_MAX_SIZE = 5000;
      const correctSizeValidatorSpy = jest.spyOn(uploader, 'hasCorrectSize');
      correctSizeValidatorSpy.mockImplementationOnce(() => false);
      const uploadFileSpy = jest.spyOn(uploader, 'uploadFile');

      const input = div.querySelector('input');
      if (!input) throw new Error();

      // create a text file
      const largeFileData = randomString(MOCKED_MAX_SIZE * 1.5);
      const largeFile = new File([largeFileData], 'chucknorris.png', {
        type: 'image/png',
      });

      //WHEN - Simulate file has been uploaded
      act(() => {
        Simulate.change(input, { target: { files: [largeFile] } } as any);
      });

      // THEN
      expect(uploadFileSpy).not.toHaveBeenCalled();
      expect(correctSizeValidatorSpy).toHaveBeenCalledWith(largeFile);
      expect(correctSizeValidatorSpy).toHaveReturnedWith(false);
      uploadFileSpy.mockClear();
      correctSizeValidatorSpy.mockClear();
    });
  });
});
