import * as React from 'react';
import { GET_PICTURES_ENDPOINT } from '../../utils/endpoints';

interface DbPicture {
  $loki: string;
  fieldname: string;
  originalname: string;
  encoding: string;
  mimetype: string;
  size: number;
  destination: string;
  filename: string;
  path: string;
  buffer: Buffer;
}
/*
0:
  $loki: 1
  destination: "uploads/"
  encoding: "7bit"
  fieldname: "file"
  filename: "763d171ced664b33667f58bb7eff890b"
  meta:
  created: 1564251033703
  revision: 0
  version: 0
  mimetype: "image/png"
  originalname: "Screenshot 2019-07-25 01.20.33.png"
  path: "uploads/763d171ced664b33667f58bb7eff890b"
  size: 91641
*/

type Pictures = ReadonlyArray<Picture>;

export interface Picture {
  readonly id: string;
  readonly name: string;
  readonly size: number;
}

export interface PicturesProviderProps {
  readonly pictures: ReadonlyArray<Picture>;
  readonly update: () => void;
}

export const PicturesContext = React.createContext<PicturesProviderProps>({
  pictures: [],
  update: (): void => {},
});

interface Props {
  readonly children: React.ReactNode;
}

async function getPictures(): Promise<ReadonlyArray<Picture>> {
  const result = await fetch(GET_PICTURES_ENDPOINT);
  const pictures: ReadonlyArray<DbPicture> = await result.json();

  const reactPictures = pictures.map(pic => ({ id: pic.$loki, name: pic.originalname, size: pic.size }));

  return reactPictures;
}

export const PicturesProvider = ({ children }: Props): JSX.Element => {
  const [pictures, setPictures] = React.useState<Pictures>([]);

  React.useEffect(() => {
    async function fetchData(): Promise<void> {
      const pictures = await getPictures();

      setPictures(pictures);
    }
    fetchData();
  }, []);

  const loadPictures = async (): Promise<void> => {
    const pictures = await getPictures();

    setPictures(pictures);
  };

  const picturesContextValue = {
    pictures,
    update: loadPictures,
  };

  return <PicturesContext.Provider value={picturesContextValue}>{children}</PicturesContext.Provider>;
};
