import * as React from 'react';
import { storiesOf } from '@storybook/react';
import { ThemeDecorator } from '../../utils/storybook';

export const FaultyComponent = (): JSX.Element => {
  throw new Error('Random error raised on children');

  return <div>I am a faulty component</div>; // eslint-disable-line
};

storiesOf('Awesome Upload App|Components', module)
  .addDecorator(ThemeDecorator)
  .add('Error Boundary', () => <FaultyComponent />);
