import React from 'react';
import { reportError } from './errorReporter';
import { Typography, Box } from '@material-ui/core';

interface State {
  readonly error: boolean;
}

// Hooks limitations https://reactjs.org/docs/hooks-faq.html#do-hooks-cover-all-use-cases-for-classes
export default class ErrorBoundary extends React.Component<{}, State> {
  public readonly state = {
    error: false,
  };

  public componentDidCatch(error: Error | null, errorInfo: any): void {
    this.setState({ error: true });
    reportError(errorInfo);
  }

  public render(): React.ReactNode {
    return this.state.error ? (
      <Box marginTop={8}>
        <Typography align="center">
          You can show here your custom error page or inherited error forms like Sentry.showReportDialog()
        </Typography>
        <Typography align="center">Please, refresh the app</Typography>
      </Box>
    ) : (
      this.props.children
    );
  }
}
