import React from 'react';
import ReactDOM from 'react-dom';
import ErrorBoundary from './ErrorBoundary';
import { act } from 'react-dom/test-utils';
import * as errorReporter from './errorReporter';
import { FaultyComponent } from './ErrorBoundary.stories';

describe('Errors :: ErrourBoundary', () => {
  let consoleSpy: jest.MockInstance<void, any>;
  beforeEach(() => {
    consoleSpy = jest.spyOn(console, 'error');
    consoleSpy.mockImplementation(() => {});
  });

  afterEach(() => {
    consoleSpy.mockRestore();
  });

  it('catches unexpected children errors', () => {
    const errorReporterSpy = jest.spyOn(errorReporter, 'reportError');
    const div: HTMLDivElement = document.createElement('div');
    act(() =>
      ReactDOM.render(
        <ErrorBoundary>
          <FaultyComponent />
        </ErrorBoundary>,
        div,
      ),
    );

    expect(errorReporterSpy).toHaveBeenCalledTimes(1);
    ReactDOM.unmountComponentAtNode(div);
  });
});
