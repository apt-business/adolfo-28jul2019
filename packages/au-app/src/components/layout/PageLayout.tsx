import * as React from 'react';
import { Box, makeStyles, Theme } from '@material-ui/core';

interface Props {
  readonly children: React.ReactNode;
}

const useStyles = makeStyles((theme: Theme) => ({
  box: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    padding: `${theme.spacing(2)}px 0`,
    margin: 'auto',
  },
}));

const PageLayout = ({ children }: Props): JSX.Element => {
  const classes = useStyles();

  return (
    <Box paddingRight={4} paddingLeft={4}>
      <Box className={classes.box} maxWidth="960px">
        {children}
      </Box>
    </Box>
  );
};

export default PageLayout;
