import { Express } from 'express';
import cors from 'cors';

const allowedOrigins = ['http://localhost:3000', 'http://localhost:9009'];

export function enableSecurityCorsOn(app: Express): void {
  app.use(function(req, res, next) {
    res.header('Access-Control-Allow-Origin', 'http://localhost:3000,http://localhost:9009');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    next();
  });

  app.use(
    cors({
      origin: function(origin, cb) {
        if (!origin) {
          // allow requests with no origin (like mobile apps or curl requests)
          return cb(null, true);
        }

        if (allowedOrigins.indexOf(origin) === -1) {
          const msg = 'CORS Policy do not allow this request to be handled in this service';

          return cb(new Error(msg), false);
        }

        return cb(null, true);
      },
    }),
  );
}
