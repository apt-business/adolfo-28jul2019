export function imageFilter(
  req: Express.Request,
  file: Express.Multer.File,
  cb: (error: Error | null, acceptFile: boolean) => void,
): void {
  if (!file.originalname.match(/\.(jpe?g|png)$/)) {
    return cb(new Error('You can only upload image files'), false);
  }

  const hasValidMime = /image\/(jpe?g|png)$/i.test(file.mimetype);
  if (!hasValidMime) {
    return cb(new Error('You can only upload image files'), false);
  }

  cb(null, true);
}
