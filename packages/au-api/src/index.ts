import express from 'express';
import RateLimit from 'express-rate-limit';
import multer from 'multer';
import { loadCollection, createDb, PICTURES_COLLECTION } from './db';
import { enableSecurityCorsOn } from './security/cors';
import { imageFilter } from './security/multer';

const app = express();
enableSecurityCorsOn(app);

const limiter = new RateLimit({
  windowMs: 15 * 60 * 1000, // 15 minutes
  max: 100,
});
app.use(limiter);

const UPLOADS_PATH = 'uploads';
const db = createDb(UPLOADS_PATH);
const upload = multer({
  dest: `${UPLOADS_PATH}/`,
  limits: { fieldNameSize: 100, fileSize: 10 * 1024 * 1024 },
  fileFilter: imageFilter,
});

app.post('/picture', upload.single('file'), async (req, res) => {
  try {
    const col = await loadCollection(PICTURES_COLLECTION, db);
    const data = col.insert(req.file);

    db.saveDatabase();
    res.send(data);
  } catch (err) {
    res.sendStatus(400);
  }
});

app.get('/pictures', async (_req, res) => {
  try {
    const col = await loadCollection(PICTURES_COLLECTION, db);
    res.send(col.data);
  } catch (err) {
    res.sendStatus(400);
  }
});

app.get('/find/:term', async (req, res) => {
  try {
    const col = await loadCollection(PICTURES_COLLECTION, db);
    const elem = col.data.find(file => file.originalname === req.params.term);
    if (!elem) {
      res.sendStatus(404);
      return;
    }

    res.sendStatus(202);
  } catch (err) {
    res.sendStatus(400);
  }
});

app.delete('/picture/:id', async (req, res) => {
  try {
    const col = await loadCollection(PICTURES_COLLECTION, db);
    const pictureToBeRemoved = col.get(req.params.id);

    if (pictureToBeRemoved) {
      col.remove(pictureToBeRemoved);
      db.saveDatabase();
    }

    res.sendStatus(204);
  } catch (err) {
    res.sendStatus(400);
  }
});

app.listen(3002, function() {
  console.log('listening on port 3002!'); // eslint-disable-line
});
