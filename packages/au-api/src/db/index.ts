import Loki from 'lokijs';

export const PICTURES_COLLECTION = 'pictures';

export function createDb(path: string): Loki {
  return new Loki(`${path}/db.json`, { persistenceMethod: 'memory' });
}
type DeepPartial<T> = { [K in keyof T]?: DeepPartial<T[K]> };
type FileType = Express.Multer.File & DeepPartial<LokiObj>;

export function loadCollection(name: string, db: Loki): Promise<Collection<FileType>> {
  return new Promise(resolve => {
    db.loadDatabase({}, () => {
      // Get or create collection
      const collection = db.getCollection(name) || db.addCollection(name);
      resolve(collection);
    });
  });
}
